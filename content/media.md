---
title: In the Media
subtitle: 
comments: false
---

# Scientific American

[World's Most Powerful Particle Collider Taps AI to Expose Hack Attacks](https://www.scientificamerican.com/article/worlds-most-powerful-particle-collider-taps-ai-to-expose-hack-attacks/)

# PwnedCR 0x3

[Private Machine Learning - Andres Gomez](https://www.youtube.com/watch?v=VTqMC6fpA10)
