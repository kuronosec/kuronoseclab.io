+++
author = "kurono"
title = "Aprendizaje de máquina como arma de guerra"
date = "2016-09-24"
description = "Aprendizaje de máquina como arma de guerra"
tags = [
    "security",
]
+++

# Aprendizaje de máquina como arma de guerra

En numerosas películas es posible ver a robots remplazando soldados en la tareas que son comunes en los campos de batalla. Robots dotados de una increíble inteligencia que combaten contra otros robots y humanos. Aunque esto es un tema recurrente en la ciencia ficción, en los últimos años la ciencia y la tecnología nos han permitido acercarnos a dicho escenario distópico.
Los años recientes han visto un progreso significativo en el área del aprendizaje de máquina, un conjunto de herramientas matemáticas que permiten emular el funcionamiento del cerebro humano, potenciado por el poder de cálculo de los grandes centros de cómputo. El objetivo es tomar una enorme cantidad de datos y hacer que éstos sean procesados iterativamente por algoritmos que se adaptan a dichos datos, obteniendo la capacidad de hacer predicciones sobre información futura.
Este mismo principio podría en teoría ser utilizado para construir los mencionados robots "soldados". Un escenario que suena bastante inverosímil y complejo, pero que ya ha sido llevado a la práctica en la vida real. Hace algún tiempo leí acerca de un programa de la NSA llamado "Skynet" [1], no por coincidencia con un nombre tomado a la popular película "Terminator".
Skynet es un programa de defensa que consiste en la recolección de metadatos de comunicaciones electrónicas, especialmente llamadas mediante dispositivos celulares, con el fin de detectar de forma "inteligente", comunicaciones realizadas por grupos terroristas. El resultado de dicha clasificación es luego utilizada para localizar a las fuentes de las comunicaciones detectadas, y correspondientemente dirigir drones para atacar sus ubicaciones.
La existencia de este programa ha sido confirmada por una serie de diapositivas que hacen parte de las filtraciones de Snowden  [2]. Naturalmente más allá del interés tecnológico que nos pudiera despertar esta noticia, surgen además muchas interrogantes éticas e incluso de fundamento científico sobre las bases de dicho programa.
Por ejemplo, en la noticia se menciona que este programa se aplica por parte de Estados Unidos sobre un país completo, presumiblemente Pakistan, entonces ¿Esta bien vigilar a toda una población de un país, incluso si es para detectar terroristas? Evidentemente un sistema de estas características podría ser abusado por un gobierno tirano para localizar además periodistas, opositores, activistas y otros. Otra pregunta importante es ¿Esta el aprendizaje de máquina en un estado suficientemente avanzado, como para confiarle la vida o muerte de personas? Según el artículo, el sistema tiene un nivel de error que lo hace inaceptable, y evidentemente se han cometido equivocaciones, clasificando y/o asesinado miles de personas inocentes. ¿Aceptarían los ciudadanos norteamericanos que un programa como este se aplique en su propio territorio?
Como reflexión final, es tentador dejarse llevar por la necesidad de la seguridad y el aprovechamiento de la tecnología para reforzarla. Sin embargo, si no se consideran las consecuencias y límites éticos y científicos, los resultados finales pueden ser mucho peor que el mal que en principio se quería resolver.

# Referencias

[1] http://arstechnica.co.uk/security/2016/02/the-nsas-skynet-program-may-be-killing-thousands-of-innocent-people/

[2] https://theintercept.com/document/2015/05/08/skynet-applying-advanced-cloud-based-behavior-analytics 
