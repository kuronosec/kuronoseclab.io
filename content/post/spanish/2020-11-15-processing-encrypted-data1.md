+++
author = "kurono"
title = "Procesando informacion cifrada - un ejemplo introductorio"
date = "2020-11-15"
description = "Procesando informacion cifrada - un ejemplo introductorio"
tags = [
    "security",
]
+++

En mi blog [1] hablé de la motivación detrás de utilizar datos cifrados de los usuarios para el entrenamiento y/o predicción de un modelo de machine learning, y así respetar su privacidad.Comenzando con este tutorial, voy a mostrar ejemplos sobre cómo usar mi librería Arhuaco en conjunto con PySyft de OpenMined para entrenar modelos basados en datos que no son visibles, y como ejecutar predicciones en esos mismos modelos. Antes recomiendo dar un vistazo a los tutoriales en PySyft [2] especialmente la introducción a procesamiento con datos cifrados [3], con una explicación simplificada de las matemáticas involucradas.
Vale aclarar que para seguir este tutorial se recomienda usar alguna distribución de Linux (Yo usé Debian Buster).Antes que nada, debemos instalar las dependencias de Python (asegurate de usar la versión 3 del lenguaje), que usaremos durante los tutoriales. Para descargar el código fuente de las librerías hacemos lo siguiente:

```bash
git clone https://gitlab.com/kuronosec/arhuaco
git clone https://github.com/OpenMined/PySyft
cd PySyft
make notebook
```

Este último comando instala PySyft en un ambiente limpio de Python (3) que se puede encontrar en PySyft/venv. Aprovecharemos ese mismo ambiente para correr los notebooks, así:

```bash
source PySyft/venv/bin/activate 
```

Finalmente, para ejecutar el actual notebook:

```bash
cd arhuaco
git checkout devel
python setup.py install
jupyter notebook
```

Luego navegar a "examples/Processing encrypted data 1 - ES.ipynb" 

## Multiplicación de vectores

En este tutorial empezaremos con un ejemplo muy sencillo del uso de PySyft, que sirve para demostrar su potencial. PySyft puede usar varias opciones de frameworks de machine learning, en este caso usaremos PyTorch [4]. 

```python
# Primero vamos a incluir dependencias y ver si todo
# funciona correctamente
import sys

# Llamar las librerías de Torch
import torch
from torch.nn import Parameter
import torch.nn as nn
import torch.nn.functional as F

# Llamar la librería de PySyft
import syft as sy
# Esta método envuelve a Torch con métodos especiales para
# el aprendizaje de máquina privado.
hook = sy.TorchHook(torch)

# creamos un tensor normal de torch
torch.tensor([1,2,3,4,5])  
```

Si todo funciona correctamente significa que nuestras dependencias están bien instaladas. Ahora proseguimos así: 

```python
# Creamos cuatro "trabajadores virtuales" con quien compartiremos nuestros datos
# de forma cifrada.
juan = sy.VirtualWorker(hook, id="juan")
ana = sy.VirtualWorker(hook, id="ana")
# El crypto provider es un nodo especial que provee
# funcionalidades criptográficas, aunque en nuestro ejemplo
# es igual a los demás trabajadores
crypto_provider = sy.VirtualWorker(hook, id="crypto_provider")  # Creamos un par de vectores que queremos multiplicar
# con valores sencillos. Nosotros, como clientes conocemos
# esos valores.
x = torch.tensor([10])
y = torch.tensor([3])  
# Ahora ciframos y enviamos esos vectores sencillos
# a los trabajadores virtuales. Ellos no deberían poder ver
# los valores en texto claro. Por supuesto, en este ejemplo
# todos los trabajadores están corriendo en la misma máquina y
# espacio de memoria, más adelante veremos como funciona
# el caso remoto.
x = x.share(juan, ana, crypto_provider=crypto_provider)
y = y.share(juan, ana, crypto_provider=crypto_provider)  # Posteriormente multiplicamos los dos vectores simple que creamos
z = x * y
# Finalmente retornamos y desciframos el resultado localmente.
z.get()
```

Al final deberíamos obtener un vector con un valor de 30, que para nosotros es visible, pero para los trabajadores virtuales estaba cifrado, es decir, ellos hicieran la operación por nosotros, pero no saben el valor de los vectores multiplicados.Este corto tutorial nos da una idea de la forma de funcionar de PySyft, cómo procesa información como vectores y matrices y nos prepara para ver casos más complejos cómo las redes neuronales, que mostraré en próximos tutoriales. Referencias

* [1] https://kuronosec.blogspot.com/2020/07/private-machine-learning-aprendizaje-de.html
* [2] https://github.com/OpenMined/PySyft/tree/master/examples/tutorials/translations/espa%C3%B1ol
* [3] https://github.com/OpenMined/PySyft/blob/master/examples/tutorials/translations/espa%C3%B1ol/Parte%2009%20-%20Intro%20a%20los%20Programas%20Encriptados.ipynb
* [4] https://pytorch.org/tutorials/
