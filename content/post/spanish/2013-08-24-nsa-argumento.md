+++
author = "kurono"
title = "La NSA y el argumento del terrorismo"
date = "2013-08-24"
description = "La NSA y el argumento del terrorismo"
tags = [
    "security",
]
+++

# La NSA y el argumento del terrorismo

El tabaco mata a casi 6 millones de personas cada año [1]. 1,24 millones de personas mueren en accidentes de tránsito [2]. El consumo bebidas alcohólicas causa 2,5 millones de muertes [3]. 500 mil personas mueren cada año por armas de fuego [4]. 15 mil mueren anualmente por terrorismo en el mundo [5].

Cada una de estas muertes es trágica sin importar quien la sufre. Entonces, podríamos preguntarnos: ¿Qué sería necesario, así fuese radical, para para salvar la vida de éstas personas? Una alternativa  sería la de prohibir el consumo de tabaco y alcohol, eso nos llevaría a salvar la vida de 8,5 millones de personas cada año. Podríamos además prohibir a las personas el uso de vehículos, lo que nos llevaría ahora a salvar 9,74 millones. Suena interesante, por qué no además, prohibir el uso de armas de fuego, entonces salvaríamos otras 500 mil vidas por año. Finalmente tendríamos que pensar una solución para el terrorismo, si hacemos caso al concepto de la NSA, entonces la solución sería abolir la privacidad de toda la humanidad.

Ahora bien, ¿Qué pasa si llevamos éstas propuestas a la realidad? Podría adivinar que habría multitudes de personas, empresas e incluso políticos absolutamente furiosos. No tardaría mucho tiempo antes de que algunos gobiernos comenzaran a caer, si se niegan a dar marcha atrás. Y tendrían razón. Todas éstas propuestas son absurdas, porque tratan de solucionar un problema creando otro, quitarle la libertad a las personas. Por supuesto sin mencionar la dificultad absoluta de llevar a cabo dichas restricciones. Ahora, si analizamos con detenimiento, dentro de los derechos que tienen las personas a fumar y a beber, a conducir vehículos, a portar armas de fuego - en algunos países -, y el derecho a llevar una vida privada, libre y sin intromisiones, el único de éstos consagrados en la declaración universal de los derechos humanos es precisamente el último. Y aunque parezca irreal, es lo que estamos viendo con programas gubernamentales como PRISM.

Así que finalmente el argumento de la NSA para la creación del programa PRISM y similares - combatir el terrorismo - es un completo sinsentido. El argumento de la seguridad - salvar vidas - contra la libertad, simplemente carece de sentido cuando es preferible sacrificar la privacidad que el derecho a beber, fumar, conducir y portar armas.

# Referencias

* [1] - https://www.who.int/es/news-room/fact-sheets/detail/tobacco
* [2] - https://www.who.int/violence_injury_prevention/road_safety_status/2013/report/summary_es.pdf
* [3] - https://www.who.int/es/news-room/fact-sheets/detail/alcohol
* [4] - https://elpais.com/internacional/2013/01/17/actualidad/1358443900_410640.html
* [5] - https://www.jornada.com.mx/2010/08/06/mundo/027n1mun
