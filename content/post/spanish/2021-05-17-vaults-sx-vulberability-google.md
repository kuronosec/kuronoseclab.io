## Vulnerabilidad encontrada en el contrato inteligente `vaults.sx` (ataque EOS SX Vault)

Una calida y bella mañana en Costa Rica, un viernes 14 de mayo, este mensaje fue posteado el grupo de Telegram de EOS Nation:

```
Estamos investigando un ataque a la bóveda. La mayoría de EOS y USDT en la bóveda han sido robados.

❗️SX ataque a la bóveda

NO DEPOSITAR en bóveda

Actualizaremos EOSX para evitar que las personas depositen más lo antes posible.

Le proporcionaremos un analisis completa tan pronto como completemos nuestra investigación.
```

Aparentemente, un atacante había secado la [bóveda SX](https://www.bloks.io/account/vaults.sx), explotando una vulnerabilidad
en sus contratos inteligentes. Posteriormente, se brindaron más detalles en el mismo grupo de Telegram:

```
EOS Nation ofrece una recompensa de 100.000 USDT al hacker de sombrero blanco que identificó el ataque de reentrada en el
contrato inteligente flash.sx.

La recompensa se transferirá a la cuenta de su elección una vez que el EOS 1,180,142.5653 y el USDT 461,796.8968 se devuelvan
a la cuenta flash.sx.
```

Parece que EOS Nation estaba tratando de convencer al atacante de que devolviera los fondos robados ofreciéndole una recompensa.
La cuenta EOS del atacante también fue revelada:

```
https://www.bloks.io/account/potghpfcmocs
```

Aunque ya existe un buen análisis del método de explotación utilizado por el atacante [1], decidimos realizar nuestro propio
análisis para aprender del error y entender cómo evitarlo en el futuro. También nuestra intención es dar muchos más detalles
involucrados que no fueron mencionados en [1], que nos costó entender. Por lo tanto, el lector puede obtener conocimientos
más profundos al respecto. Además, este tipo de ataques importantes necesitan una visión y un análisis de diferentes
ojos nuevos, para poder mejorar la seguridad de los sistemas involucrados.

El contrato inteligente de la víctima se puede encontrar aquí: [`vaults.sx`](https://github.com/stableex/sx.vaults). Allí,
"los usuarios pueden enviar tokens EOS a` vaults.sx` para recibir tokens SXEOS. Y "los usuarios pueden enviar tokens
SXEOS a` vaults.sx` para recibir su EOS + cualquier interés acumulado durante el período de tiempo en que se mantiene el
activo SXEOS ". El otro contrato utilizado en el ataque fue [`flash.sx`](https://github.com/stableex/sx.flash) donde los
usuarios" piden prestada cualquier cantidad de liquidez al instante por tarifas cercanas a cero y sin garantía ".

Usamos el [Dfuse explorer](https://eos.eosq.eosnation.io) para rastrear las actividades del atacante. Dado que EOS, como
muchas de las redes de cadenas de bloques más populares, es pública, todas las actividades, como las transacciones y las
llamadas a la acción, son visibles públicamente. Esto ayuda mucho a realizar un análisis forense. A continuación,
damos detalles sobre lo que podríamos deducir de los movimientos del atacante y las vulnerabilidades explotadas:

* El atacante `potghpfcmocs` deposita un determinado token, como USDT, y recibe el token alternativo SXUSDT, todo como siempre.

```bash
# Solo un ejemplo
potghpfcmocs → vaults.sx 2 USDT
vaults.sx → flash.sx 2 USDT
token.sx emitió 20 SXUSDT a vaults.sx
vaults.sx → potghpfcmocs 20 SXUSDT
```

Este conjunto de acciones se llevan a cabo dentro de `vaults.sx`, en
la [función `on_transfer`](https://github.com/stableex/sx.vaults/blob/main/vaults.sx.cpp#L68). Esta función monitorea
la transacción entrante en todos los tokens involucrados: `[[eosio :: on_notify (" * :: transfer ")]]`. La parte de
depósito de la acción se procesa en el primer condicional:

```C ++
// depósito - manejar la emisión (ej: EOS => SXEOS)
if (deposit_itr! = _vault.end ()) {
```

* Ahora que el atacante tiene los fondos de token alternativos, puede transferirlos al contrato `vaults.sx` para obtener
el token original, que en teoría habría cobrado intereses a lo largo del tiempo, pero el atacante lo hace todo de inmediato:

```bash
potghpfcmocs → bóvedas.sx 10 SXUSDT
```

Sin embargo, esta transferencia no es procesada directamente por "vaults.sx". Primero va a las acciones de transferencia
del [`token.sx`] personalizado (https://github.com/stableex/sx.token/blob/master/token.sx.cpp#L85) que administra `SX...`
fondos. Esta sección de código agrega tanto el atacante como `vaults.sx` como cuentas a las que se les notificará cuando
se completen las acciones de transferencia [2]:

```C ++
require_recipient (desde);
require_recipient (a);
```

La sección de código anterior devuelve a la cuenta del atacante el control del flujo de ejecución antes de que el contrato
inteligente del destinatario pueda realizar cualquier actualización de estado. ¡Aquí es donde se puede explotar la
vulnerabilidad! En EOSIO, las notificaciones (invocadas por `require_recipient`) envían una copia de la transacción actual
a las cuentas involucradas. Las cuentas de los destinatarios pueden luego realizar algún procesamiento basado en dichas
notificaciones. Eso es lo que sucede en `on_transfer` del contrato` vaults.sx`. Sin embargo, las funciones invocadas
también pueden invocar acciones en otros contratos denominados acciones en línea. El orden en el que se llaman las
notificaciones y las acciones en línea en EOSIO es el siguiente:

Todas las notificaciones y su procesamiento interno se ejecutan primero. Luego, si las funciones que administran las
notificaciones invocan acciones en línea, se ejecutan después de todas las notificaciones.

En primer lugar, las acciones en profundidad, es decir, todas las acciones en línea de la primera cuenta notificada se
ejecutarán primero, luego las de la segunda y así sucesivamente.

Para comprender las implicaciones de esto para la vulnerabilidad actual, primero echemos un vistazo a cómo se vería el
flujo de ejecución normal dentro de `vaults.sx`. La acción `on_transfer` recibiría la notificación, pero en este caso se
seguiría la parte de retirada del condicional:

```C ++
// retirar - manejar retirar (ej: SXEOS => EOS)
} else if (supply_itr! = _vault_by_supply.end ()) {
```

en este apartado del condicional se ejecuta una importante operación para la explotación:

```C ++
const extendido_asset out = calculate_retire (id, cantidad);
```
```
Extended_asset sx :: vaults :: calculate_retire (id de código de símbolo const, pago de activo const)
...
const int64_t S0 = vault.deposit.quantity.amount;
const int64_t R0 = vault.supply.cantity.amount;
const int64_t p = (uint128_t (monto.pago) * S0) / R0;

return {p, vault.deposit.get_extended_symbol ()};
```

La cantidad de fondos que el usuario puede retirar es proporcional a la cantidad actual de depósito del token específico
guardado en la bóveda. Después de esto, la cantidad calculada se transfiere al usuario y se resta del depósito disponible:

```C ++
// actualizar depósito y suministro interno
_vault_by_supply.modify (supply_itr, get_self (), [&](automático y fila) {
    row.deposit - = fuera;
    row.supply.quantity - = cantidad;
    row.last_updated = current_time_point ();
```

Finalmente, los fondos calculados serían devueltos al usuario:

```C ++
transferir (cuenta, get_self (), out, get_self (). to_string ());
// enviar activos subyacentes al remitente
transferir (get_self (), desde, fuera, get_self (). to_string ());
```

Todo genial, ¡pero espera un momento! Antes dijimos que si la función notificada llamaba acciones en línea
(como una transferencia), tendría que esperar hasta que todas las acciones en línea del primer notificado
(el atacante) hubieran terminado su ejecución. Esto significa que en este punto, `vaults.sx` ha cambiado su estado
interno como si hubiera transferido el fondo al usuario, pero en realidad la transferencia aún no ha ocurrido y
tampoco los fondos han sido recolectados de` flash.sx` . El atacante, el primero en ser notificado, se aprovecha
de esta inconsistencia invocando la acción "pedir prestado" del contrato "flash.sx":

```bash
flash.sx - pedir prestado 0.0001
flash.sx → potghpfcmocs 0.0001 USDT
potghpfcmocs → flash.sx 0.0002 USDT
vaults.sx - ID de actualización: USDT
```

Esto sucede [aquí](https://github.com/stableex/sx.flash/blob/master/flash.sx.cpp#L22). `flash.sx` presta una cierta cantidad
de fondos al usuario y verifica que el usuario haya devuelto la misma cantidad o una mayor antes de que finalice la acción
de` pedir prestado`. No hay explotación directa en esta acción y todo sale como se esperaba. Sin embargo, la parte interesante
llega cuando la acción llama a la acción `update` en el contrato` vaults.sx`:

```C ++
// obtener saldo de la cuenta
balance de activos const = eosio :: token :: get_balance (contrato, cuenta, código simbólico ());
...
// actualizar el saldo
_vault.modify (vault, get_self (), [&](automático y fila) {
    row.deposit.quantity = saldo + apostado;
    row.staked.quantity = staked;
    row.last_updated = current_time_point ();
});
```

El contrato `vaults.sx` está actualizando el balance de estado interno basado en el balance actual de `flash.sx` que aún no
se ha reducido. Después de que finaliza la acción de "pedir prestado", el control todavía está en el lado atacado, por lo
que envía otra transferencia SXUSDT a "vaults.sx", esta vez sin interrumpir el flujo normal. Si echamos un vistazo de
nuevo a `calculate_retire` en` vaults.sx`:

```C ++
const int64_t S0 = vault.deposit.quantity.amount;
const int64_t R0 = vault.supply.cantity.amount;
const int64_t p = (uint128_t (monto.pago) * S0) / R0;
```

por lo tanto, tenemos una situación con un depósito inflado (dada la actualización de estado incorrecta) y un suministro
que no se ha reducido en consecuencia. A medida que avanza la ejecución:

```bash
// Solo un ejemplo
potghpfcmocs → bóvedas.sx 10 SXUSDT
vaults.sx → potghpfcmocs 2 USDT
```

el atacante retira una cantidad inflada, dada la actualización incorrecta. ¡De ahí que reciba los beneficios del ataque !.
Después de que todas las acciones en línea del contrato del atacante terminan su ejecución, el control se devuelve al
segundo notificado (`vaults.sx`), y se finaliza el primer canje:

```bash
// Solo un ejemplo
token.sx - cantidad de retiro: 10 SXUSDT
vaults.sx → potghpfcmocs 1 USDT
token.sx - cantidad de retiro: 10 SXUSDT
```

Esta vez, dado que el retiro se calculó antes del ataque con un estado normal, la cantidad es lo que habría canjeado
normalmente. Al final, el estado interno se actualiza con los valores adecuados.

Dado que este mismo ataque se puede realizar automáticamente miles de veces en cuestión de segundos, el atacante logró
robar 1,180,142.5653 EOS y 461,796.8968 USDT. Sin embargo, algunas horas después del incidente, este mensaje se publicó en
el grupo EOS Nation Telegram:

```
Los productores de bloques llegaron a un consenso para defender la intención del código.

Aproximadamente 1.2M EOS y 462,000 USDT fueron robados en un exploit de ataque de reentrada en el contrato inteligente de
préstamo flash flash.sx que comenzó el 14 de mayo a las 11:28 UTC.

Los contratos inteligentes de vaults.sx y flash.sx eran de código abierto, MSIGed y pasaron las auditorías de seguridad,
sin embargo, no se identificó el exploit de reentrada.

Todos los fondos están seguros bajo el control de eosio.prods y serán devueltos a los depositantes.
```

Por lo tanto, si todos los fondos y las posibles cuentas creadas se rastrearon con éxito, el atacante se quedó sin nada de
los fondos robados. Sin embargo, esta es una medida extrema que a los productores de bloques les gustaría evitar y no
debe llevarse a cabo en todos los incidentes de seguridad en la cadena de bloques EOS.

Se puede observar un buen ejemplo de una buena secuencia de seguimiento del ataque
[aquí](https://cc32d9.medium.com/eosio-contract-security-cookbook-20210527-69797efe9c96). Aunque esto ha sido nombrado como
un exploit de ataque de reentrada, podríamos argumentar que este es un escenario diferente. En una vulnerabilidad de
reingreso típica, un contrato haría una transferencia al atacante, antes de actualizar realmente el estado interno del
contrato. El atacante entonces llamaría recursivamente la misma acción. Lo que tenemos aquí es en realidad lo contrario.
Tal vez esa sea la razón por la que las auditorías de seguridad no pudieron detectar el error `vaults.sx`. El problema aquí
parece más relacionado con la aplicación que administra el estado interno en dos contratos diferentes que se llaman entre sí,
y el flujo de ejecución de EOSIO que puede interrumpir el contrato en medio de una actualización del estado interno entre los
dos contratos involucrados.

Para evitar este tipo de error en el futuro, los desarrolladores deben tener cuidado de que una interrupción del flujo no
interfiera en medio de una actualización de estado que un atacante pueda controlar. Quizás administrar las actualizaciones de
estado en un solo contrato y dentro de una sola acción atómica sería una buena solución.

Puede encontrar un buen conjunto de prácticas de desarrollo seguro de EOSIO aquí [3].

* Referencias:

* [1] https://cmichel.io/eos-vault-sx-hack/
* [2] https://chainsecurity.com/the-dispatcher-first-line-of-defense-in-any-eos-smart-contract/
* [3] https://cc32d9.medium.com/eosio-contract-security-cookbook-20210527-69797efe9c96
