+++
author = "kurono"
title = "La uso de Bitcoin en Colombia"
date = "2014-03-30"
description = "La uso de Bitcoin en Colombia"
tags = [
    "security",
]
+++

# La uso de Bitcoin en Colombia


La superintendencia financiera de Colombia en su CARTA CIRCULAR 29 DE 2014 [1], lanza varias advertencias con respecto al uso de Bitcoin en Colombia. A continuación realizo un análisis del documento:

El documento afirma: 
" Las monedas virtuales no se encuentran respaldadas por activos físicos, por un banco central, ni los activos o reservas de dicha autoridad, por lo que el valor de intercambio de las mismas podría reducirse drásticamente e incluso llegar a cero. Por lo anterior, las personas se exponen a altas volatilidades en el precio del instrumento, dada la amplia especulación que se mantiene". 
 
Ninguna moneda moderna se encuentra respaldada por activos físicos. Hace más de medio siglo las reservas de oro soportaban los activos de moneda de un país (suponiendo que el valor del oro no es igual de subjetivo). Sin embargo, hoy en día el valor del dinero se mide en deuda, la cantidad de dinero que deben las personas y la probabilidad de que paguen o no. Todo valor monetario se basa en la percepción de las personas y su nivel de confianza en el sistema. Uno muy bien podría transar con bananos, manzanas o mangos si se dispone de la suficiente confianza en estos medios. Así mismo todas las monedas tienen riesgo de volatilidad y que se su valor llegue a cero (¿Recordamos acaso lo que paso con la crisis mundial de 2008 y lo que pasa con el Bolivar en Venezuela?). Bitcoin es igual en este sentido, la moneda virtual tendrá el valor que las personas le asignan, con una diferencia, no existen entidades centrales ni grandes corporaciones privadas que puedan alterar arbitrariamente su valor, todo se autorregula dependiendo del mercado.

Por otro lado el documento continua:
"Ninguna de las plataformas transaccionales, ni comercializadores de las “monedas virtuales" como el Bitcoin se encuentran reguladas por la ley colombiana. Tampoco se encuentran sujetas al control, vigilancia o inspección de esta Superintendencia. Por lo anterior, tales plataformas pueden no contar con estándares o procesos seguros y de mitigación de riesgos, por lo que con regularidad presentan fallas que llevan a que los usuarios de las mismas incurran en pérdidas. Un ejemplo de lo anterior, se evidencia con MT. Gox, una de las plataformas transaccionales conocidas de moneda virtual, que cerró recientemente, generando grandes pérdidas a los usuarios."

MT. Gox o cualquier entidad de intercambio de Bitcoins por dolares no son lo mismo que Bitcoin. Son entidades que utilizan la moneda virtual para su modelo de negocio y como todas tienen riesgos. Realizar compras por internet con tarjetas de crédito válidas tienen exactamente el mismo riesgo. Una persona siempre está expuesta a recibir algo por debajo de sus expectativas o no recibir absolutamente nada. Y la superintendencia tiene exactamente las mismas limitaciones para regular sobre una tienda virtual fuera de Colombia. Además, todas las personas están expuestas al robo de su dinero, ya sea físico o digital.

Otro aparte:
" Las transacciones en las plataformas son anónimas, por lo que el uso de “monedas virtuales” se puede prestar para adelantar actividades ilícitas o fraudulentas, incluso para captaciones no autorizadas de recursos, lavado de dinero y financiación del terrorismo. De acuerdo con información pública divulgada en los medios de comunicación, algunos administradores de plataformas transaccionales y de portales de venta de mercancía que vienen utilizando “monedas virtuales” como medio de pago de operaciones han sido acusados por conductas relacionadas con el uso que se le ha dado a estos instrumentos. Los compradores o vendedores de “monedas virtuales” se exponen a riesgos operativos, principalmente a que las billeteras digitales sean robadas (hackeadas), tal como ya ha ocurrido; y a que las transacciones no autorizadas o incorrectas no puedan ser reversadas."

¿Alguien quiere hacer el ejercicio de aplicar todas las afirmaciones anteriores al peso colombiano, cualquier otra moneda o medio de pago virtual, y decir si encuentra una sola diferencia? Las monedas "aceptadas" se han prestado, prestan y prestarán para actividades ilícitas y fraudulentas, lavado de activos, financiación del terrorismo, etc. Así mismo cualquier entidad "avalada" puede ser víctima de un ataque informático. ¿Cuantas personas en Colombia fueron víctimas de delitos informáticos por el uso de tarjetas de crédito? Más de una si me preguntan. Lamentablemente pasa lo mismo con Bitcoin, así como en cualquier organización y sistema detrás de los cuales haya personas y activos que tengan un valor para alguien. Bitcoin es una obra de ingeniería que utiliza los últimos adelantos en criptografía para proveer a las personas de un medio para realizar transacciones comerciales descentralizadas, sin la necesidad de un intermediario o entidad central. Como tal no pretende resolver todos los problemas de criminalidad que atacan cualquier medio de intercambio económico, simplemente porque estos persistirán mientras detrás de ellos hallan seres humanos.

En conclusión, la CIRCULAR 29 DE 2014 es un escrito sesgado y desinformado, que no aporta nada a los ciudadanos Colombianos, simplemente pretende generar miedo sobre tecnologías emergentes, con argumentos completamente absurdos. Mientras otros países aprenden a utilizar estas nuevas tecnologías para traer beneficios a sus ciudadanos, nuestras instituciones y gobierno aun se mantienen en la época de la inquisición.

# Referencias

* [1] - https://www.superfinanciera.gov.co/descargas?com=institucional&name=pubFile1007286&downloadname=cc29_14.doc
