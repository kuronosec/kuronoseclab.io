+++
author = "kurono"
title = "Registro de SIM en Colombia: en contravía con los derechos a la privacidad"
date = "2015-06-02"
description = "Registro de SIM en Colombia: en contravía con los derechos a la privacidad"
tags = [
    "security",
]
+++

# Registro de SIM en Colombia: en contravía con los derechos a la privacidad y anonimato en la era digital


Recientemente se publicó un estudio titulado "Report on encryption, anonymity, and the human rights framework" [1], un documento de la Naciones Unidades escrito por David Kaye, que describe la situación global actual de los derechos fundamentales de libertad de expresión, la privacidad y anonimato en la era digital. Este documento concluye que el uso de herramientas de comunicación segura utilizando método de cifrado, así como la posibilidad de cualquier individuo de comunicarse de forma anónima, son mecanismo necesarios de protección de estos derechos humanos fundamentales. Para llegar a esta conclusión, se utilizan ejemplos de normas y mecanismos tecnológicos usados por algunos gobiernos para ejercer un control sobre las medios digitales dentro y fuera de sus territorios. 

Aunque en el caso colombiano hay muchas situaciones dignas de mencionar [2], [3], [4], el documento de las Naciones Unidas describe una en particular relacionado con el registro obligatorio de la tarjeta SIM, utilizada para la operación de los teléfonos móviles. Textualmente:

" ... Likewise, Governments often require SIM card registration; for instance, nearly 50 countries in Africa require or are in the process of requiring the registration of personally identifiable data when activating a SIM card. Colombia has had a mandatory mobile registration policy since 2011, and Peru has associated all SIM cards with a national identification number since 2010. Other countries are considering such policies. Such policies directly undermine anonymity, particularly for those who access the Internet only through mobile technology. Compulsory SIM card registration may provide Governments with the capacity to monitor individuals and journalists well beyond any legitimate government interest.
1 Kevin P. Donovan and Aaron K. Martin, “The Rise of African SIM Registration”, 3 February 2014.
2 See Colombia, Decree 1630 of 2011; Perú 21, Los celulares de prepago en la mira, 27 May 2010."

El texto describe la obligatoriedad del registro de tarjetas SIM en algunos países de África, Colombia y Perú.  Se menciona que dicha políticas atacan directamente el derecho al anonimato, especialmente para aquellos que solo acceden a internet a través del celular.  Además, proveen a los gobiernos de una herramienta eficaz de monitoreo sobre individuos, incluidos periodistas. 

Propuesto como una solución al robo de teléfonos móviles en Colombia, el decreto 1630 de 2011 [5], que obliga a las operadores de telefonía móvil a mantener registros que asocian tarjetas SIM con la verdadera identidad del propietario, ha dado muestras de ser ineficaz en este sentido [6]. Sin embargo éstas bases de datos, como menciona el informe de las Naciones Unidas, son una herramienta invaluable para el control sobre los ciudadanos, incluso aquellos que no son blanco de organismos de inteligencia o judiciales.

Concluyo con una cita del mismo informe:
" ... but emergency situations do not relieve States of the obligation to ensure respect for international human rights law."


# Referencias

[1] http://www.ohchr.org/EN/Issues/FreedomOpinion/Pages/CallForSubmission.aspx
[2] https://www.youtube.com/watch?v=VsClj8w4Liw
[3] http://www.semana.com/opinion/articulo/la-corte-constitucional-prefiere-la-censura-opinion-emmanuel-vargas-penagos/428983-3
[4] http://www.elcolombiano.com/relevan-a-20-militares-y-retiran-a-cinco-por-casos-andromeda-y-hacker-andres-sepulveda-MX1154169
[5] http://www.mintic.gov.co/portal/604/w3-article-3558.html
[6] http://www.elcolombiano.com/en_2013_subio_el_robo_de_celulares-HAEC_277022


