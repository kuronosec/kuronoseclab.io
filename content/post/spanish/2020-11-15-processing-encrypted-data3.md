+++
author = "kurono"
title = "Extrayendo información de datos sensibles y privados - parte B"
date = "2020-11-15"
description = "Extrayendo información de datos sensibles y privados - parte B"
tags = [
    "security",
]
+++

Una vez que los trabajadores de la parte A [1] de este tutorial se encuentran escuchando por nuevas peticiones, procedemos a emular las peticiones cifradas por parte del cliente. 

```python
# Primero importamos todas la librerías necesarias para crear nuestra red
# neuronal.
from __future__ import print_function

import pickle
import tensorflow.keras as keras
from tensorflow.keras.datasets import boston_housing
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, AveragePooling2D
from tensorflow.keras.layers import Activation

import numpy as np
import tensorflow as tf
import syft as sy
hook = sy.KerasHook(tf.keras)  
```

Debemos crear la misma estructura de red neuronal usada en el servidor. 

```python
num_classes = 1
batch_input_shape = (1, 13)
batch_output_shape = (1, num_classes)  # Nuestra meta es mostrar la predicción de datos cifrados, así que vamos a usar
# los mismos datos de prueba en lugar de datos nuevos. Cargamos de nuevo la base de datos.
(x_train, y_train), (x_test, y_test) = boston_housing.load_data()  # pre-procesamiento
mean = x_train.mean(0, keepdims=True)
dev = x_train.std(0, keepdims=True)
x_train = (x_train - mean) / dev
x_test = (x_test - mean) / dev  # Creamos un conjunto de clientes que se conectaran con los trabajadores de la parte A
# del tutorial.
client = sy.TFEWorker()

alice = sy.TFEWorker(host='localhost:4000')
bob = sy.TFEWorker(host='localhost:4001')
carol = sy.TFEWorker(host='localhost:4002')
cluster = sy.TFECluster(alice, bob, carol)

client.connect_to_model(batch_input_shape, batch_output_shape, cluster)  # Escogemos tres datos de prueba.
num_tests = 3
houses, expected_labels = x_test[:num_tests], y_test[:num_tests]  # Ciframos y enviamos cada dato de entrada al servidor, el cual nos retornara una respuesta también cifrada
# (No puede ver que datos le estamos enviando). Luego desciframos la respuesta y obtenemos nuestra predicción
# de precio medio. En un ejemplo mas realista, esto significa que podemos calcular el precio de una casa 
# (la nuestra?), sin que quien calcula ese precio conozca las características de la casa! 
# A si mantenemos la información privada :)
for house, expected_price in zip(houses, expected_labels):

    res = client.query_model(house.reshape(batch_input_shape))
    print("The calculated price for the house is: %s" % res)
    print("The expected price for the house is: %s" % expected_price)  
```

Como vimos en este tutorial, el machine learning privado tiene el potencial de ayudarnos a resolver problemas del mundo real, incluso con datos que los usuarios no preferirían revelar, sin afectar la privacidad de los usuarios. Referencias

* [1] https://gitlab.com/kuronosec/arhuaco/-/blob/devel/examples/Processing%20encrypted%20data%202a%20-%20ES.ipynb
