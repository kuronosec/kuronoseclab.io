+++
author = "kurono"
title = "Protegiendo tu seguridad móvil en un ambiente adversario"
date = "2017-05-14"
description = "Protegiendo tu seguridad móvil en un ambiente adversario"
tags = [
    "security",
]
+++

# Protegiendo tu seguridad móvil en un ambiente adversario

En una de mis entradas anteriores [1], describí cómo el gobierno colombiano decidió construir, en conjunto con las empresas de prestación de servicios de telefonía celular, una base de datos de dispositivos móviles usados en el país y sus respectivos dueños como requisito para que estos puedan funcionar en le territorio nacional. Esta base de datos contiene por tanto millones de números telefónicos, números IMEI y datos como nombre de los subscriptores y localización (dirección de residencia). Más allá de los riesgos que por parte del gobierno se pueda utilizar estos datos de forma inapropiada, por ejemplo para espiar a periodistas y opositores, también está el riesgo asociado a grupos de delincuencia organizada que se dediquen a actividades de cyber ataques con fines económicos. Como recientemente vimos con el caso del malware WannaCry [2,3], el ataque fue posible gracias al robo de datos guardados por la NSA, por parte de cyber delincuentes. Esta base de datos contenía información no publicada sobre vulnerabilidades en Windows que fueron aprovechadas para lanzar un ataque a nivel global. Esto demuestra que incluso la agencia que mas dinero gasta en seguridad en el mundo, no está libre de ser victima de robo de datos. De esta manera es pertinente preguntarnos, ¿Qué tan segura podría ser esta gran base de datos de dispositivos móviles registrados en Colombia? Hasta donde se no existen documentos públicos que certifiquen su nivel de seguridad. En cualquier caso es lógico pensar que tarde o temprano se produzcan filtraciones.

A pesar de este panorama pesimista, donde incluso los gobiernos ayudan a hacer de Internet un lugar menos seguro, es posible tomar medidas para protegerse de ataques y robos de información personal. En esta y próximas entradas voy a describir un conjunto de herramientas para dispositivos móviles que nos ayudan a conservar un cierto nivel de seguridad en un ambiente altamente adversario. Sin embargo antes de proceder, el primero consejo, muy de la mano de lo pasado con WannaCry, es actuar con sentido común. Comenzando por no abrir archivos que provengan de orígenes desconocidos. Así como uno no deja entrar a su casa a personas desconocidas, uno no debe abrir archivos en su dispositivo que provengan de personas u entidades desconocidas. Siempre teniendo presente el anterior consejo, podemos proceder a revisar una lista de herramientas que al ser usadas en dispositivos móviles, ayudan a proteger su seguridad, incluso en ambientes altamente adversarios cómo el Internet actual y las redes de telefonía. Por ahora solo nombraré las herramientas, en entradas posteriores las describiré en detalle:

Navegación anónima: 

* Tor, Torbot, Torbrowser [4]
* I2p [5]

Comunicación segura:

* Signal [6]
* ChatSecure [7]
* CryptoCat [8]

Endurecer del sistema operativo y protección de datos locales:

* Cifrado de disco
* Kernel hardening

 ¡Y de seguro muchas más! :D Conforme vaya escribiendo entradas, iré añadiendo herramientas que considere pertinentes o que sean alternativas a las mencionadas.

* [1] https://kuronosec.blogspot.de/2015/06/registro-de-sim-en-colombia-en.html
* [2] http://www.elespectador.com/tecnologia/si-buscan-culpables-del-ciberataque-mundial-miren-hacia-la-nsa-articulo-693564
* [3] http://www.elcolombiano.com/tecnologia/ciberataque-global-afecta-a-cientos-de-paises-CA6525411
* [4] https://es.wikipedia.org/wiki/Tor_(red_de_anonimato)
* [5] https://es.wikipedia.org/wiki/I2P
* [6] https://es.wikipedia.org/wiki/Signal_(software)
* [7] https://chatsecure.org/
* [8] https://es.wikipedia.org/wiki/Cryptocat
