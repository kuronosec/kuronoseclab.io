+++
author = "kurono"
title = "Surveillance States: Colombian Chapter"
date = "2014-05-22"
description = "Surveillance States: Colombian Chapter"
tags = [
    "security",
]
+++

# Surveillance States: Colombian Chapter

Hace poco participe en el DragonJAR Security Conference 2014, un evento de seguridad "made in Colombia", donde tuve la oportunidad de conocer grandes personas y disfrutar de charlas tremendamente interesantes. Aquí un poco mas de información sobre el evento:

[Asi fue el dragonjar security conference 2014 dia i](https://www.dragonjar.org/asi-fue-el-dragonjar-security-conference-2014-dia-i.xhtml)

[Asi fue el dragonjar security conference 2014 dia ii](https://www.dragonjar.org/asi-fue-el-dragonjar-security-conference-2014-dia-ii.xhtml)

Precisamente en el segundo día fue el turno de mi charla "Surveillance States: Colombian Chapter", de la cual hago una pequeña referencia:

La revelaciones realizadas por Edward Snowden en 2013 terminaron por confirmar el estado de vigilancia en el que se encuentran las comunicaciones a nivel global. Aunque ya era bien conocido el papel que juegan estados como Rusia y China en este tipo de interceptaciones, los alcances revelados de la NSA Estadounidense son alarmantes, llevando casi al rechazo total del derecho fundamental a la privacidad. Colombia no se encuentra aislada de este problema, en los últimos años se han observado numerosos escándalos relacionados con interceptaciones ilegales que al fin y al cabo no son más que violaciones de los derechos humanos. Si añadimos a esto las legislaciones en materia de “inteligencia” que se han ido redactando en los últimos años, el escenario colombiano se presenta bastante complejo.
En esta charla se analiza el estado actual de la vigilancia electrónica en Colombia desde el punto de vista legislativo hasta los detalles técnicos generales. Se describe como estos elementos podrían ser abusados con facilidad. Además se muestra cómo en este emergente escenario de comunicaciones inseguras por legislación es necesario para desarrolladores y expertos en seguridad adoptar las herramientas criptográficas como elementos de uso diario. Finalmente se exponen algunas herramientas que pueden ayudar a preservar la privacidad aun en escenarios altamente vigilados, como por ejemplo la distribución de Linux Tails.

A continuación adjunto la dispositiva completa de mi presentación:

[Slides](http://www.slideshare.net/kuronosec/djcon-surveillance)

Y adicionalmente todo el material de referencia que utilice, entre ellos los contratos publicos que describen los sistemas de vigilancia usados en Colombia, que tambien se pueden encontrar en https://www.contratos.gov.co/
