+++
author = "kurono"
title = "Machine leaning privado"
date = "2020-07-20"
description = "Machine learning privado"
tags = [
    "security",
]
+++

# Private Machine Learning: Aprendizaje de maquina usando datos cifrados

El mundo digital de hoy tiene un apetito insaciable por la información de las personas. Cada vez más, diferentes actividades se realizan a través de una red abierta como Internet, algo que se ha acelerado con la actual pandemia. Como ejemplo común, muchas empresas quieren conocer los gustos de los usuarios para focalizar sus campañas de marketing y obtener mayores ventas. Sin embargo, las personas tienen derecho a tener control sobre quien y por qué alguien más accede sus datos personales al usar Internet. Estos derecho son importantes en todos los aspectos, pero en adicionalmente en áreas como la medicina y finanzas, existen leyes mas estrictas para la protección de datos. Un ejemplo de marco legal que ha supuesto una revolución para protección de información personal es el proyecto europeo regulación general de protección de datos (GDPR) [1].    

Una de las tecnologías que ha estado en centro de este apetito por información de las personas es el aprendizaje de maquina o machine learning (ML). ML es un conjunto de herramientas basadas en teorías matemáticas que permiten analizar datos y extraer respuestas a preguntas relacionadas con esos datos. Por ejemplo, basados en un conjunto de frases escritas por muchas personas es posible dividir esa frases en varios conjuntos, tal como de sentimientos positivos o negativos [2]. Otro ejemplo popular, son los sistemas de recomendaciones como los usados en Youtube o Netflix [3] para mostrar a sus usuarios contenido "similar" al observado anteriormente. Ejemplos de mayor impacto pueden encontrarse en la medicina, con algoritmos capaces de producir un diagnostico médico basados en información de los pacientes, incluso en algunos avances recientes, con resultados superiores los propios médicos [4].

El último ejemplo mencionado es una demostración de que hay casos muy válidos para la recolección y uso de datos de persona para resolver sus problemas, incluyendo casos como el seguimiento de pacientes durante la actual pandemia [5]. Hay otros casos, como la captación masiva de datos para el marketing e incremento de ventas de empresas, que muestran un menor o nulo beneficio para las personas dueñas de esos datos. En cualquiera de los dos extremos, la perdida de privacidad de las personas se ha convertido en un problema apremiante en busca de soluciones.

Avances recientes en las áreas de ML, seguridad y criptografía permiten lograr un balance entre la resolución de problemas reales para las personas y la minimización de la visibilidad sobre sus datos individuales. Por ejemplo, OpenMined (https://www.openmined.org/) [6] es un proyecto que ha desarrollado un conjunto de librerías que facilitan el desarrollo y despliegue de modelos basados en frameworks como Torch y TensorFLow, pero con adiciones para procesar datos con diferentes técnicas que mejoran el tratamiento de datos de forma privada, como federated learning, differential privacy y multi-party computing.

Algunas de éstas técnicas permiten incluso que usuarios usen modelos de ML cifrados, no teniendo acceso a los parámetros. Por otro lado, también permiten que el dueño de un modelo procese datos de usuarios que están cifrados, de manera que no pueden ser vistos, pero aún así permiten extraer predicciones precisas. Básicamente magia matemática! ;-)

El objetivo principal de éstas librerías, es el de abstraer las técnicas de protección de datos basadas principalmente en criptografía, y hacerlas más accesible a los programadores y científicos de datos acostumbrados a librerías como Torch. Al final los usuarios podrían usar frameworks de ML, sin preocuparse mucho sobre la técnicas de privacidad usados internamente. En este enlace se pueden ven algunos tutoriales, con formato de Python notebooks, que enseñan conceptos y ejemplos de uso de estas librerías: https://github.com/OpenMined/PySyft/tree/master/examples/tutorials. En  próximos posts escribiré ejemplos de algoritmos de ML usados para obtener respuestas a partir de datos cifrados, usando tećnicas como secure multi-party computation.

# Referencias:
* [1] https://gdpr.eu/
* [2] https://www.kaggle.com/drscarlat/imdb-sentiment-analysis-keras-and-tensorflow
* [3] https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/45530.pdf
* [4] https://www.nature.com/articles/s41586-019-1799-6
* [5] https://github.com/corona-warn-app
* [6] https://github.com/OpenMined/PyZPK

