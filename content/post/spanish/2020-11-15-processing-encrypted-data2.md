+++
author = "kurono"
title = "Extrayendo información de datos sensibles y privados - parte A"
date = "2020-11-15"
description = "Extrayendo información de datos sensibles y privados - parte A"
tags = [
    "security",
]
+++

En nuestro primer tutorial [1], vimos una introducción al procesamiento privado de datos, su importancia y el uso básico de la librería PySyft para procesar datos cifrados. También vimos como instalar el software necesario, que también usaremos durante este tutorial.Aquí veremos un caso práctico de machine learning (ML) privado, vamos a entrenar un modelo con datos que muchas personas preferirían mantener en secreto. Como ejemplo usaremos de la conocida (y pública) base de datos de "Boston Housing". Sin embargo, mostraremos como un usuario podría usar un modelo para obtener predicciones útiles sin exponer su información. Inicialmente haremos el entrenamiento sobre los datos en texto claro, sin cifrar, mientras que ofreceremos un servicio de predicción con "nuevos" datos cifrados.Usaremos la librerías Keras [2] y PySyft, para cargar y procesar los datos de la base de datos. En la parte A del tutorial, entrenaremos la una neural y serviremos peticiones cifradas por medio de un servidor, mientras que en la parte B crearemos el cliente y realizaremos las peticiones cifradas de nuevas predicciones. Cargar la base de datos
El "Boston Housing Dataset" [3], es un conjunto de datos tomados de un censo sobre hogares y su respectivo valor medio en miles de USD, usados tradicionalmente para probar algoritmos de ML. Se usa comúnmente para predecir el valor medio de una casa con base a ciertas características del hogar.A continuación procederemos a cargar dicha base de datos: 

```python
# Primero importamos las librerías que vamos a usar.

from __future__ import print_function

import pickle
import tensorflow.keras as keras
from tensorflow.keras.datasets import boston_housing
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, AveragePooling2D
from tensorflow.keras.layers import Activation

# Declaramos algunos parámetros para nuestra red neuronal.
batch_size = 10
test_batch_size = 10
lr = 0.001
num_classes = 1
epochs = 100
input_shape = (13,)
batch_input_shape = (1, 13)
batch_output_shape = (1, num_classes)  Procedemos con la carga de la base de datos de Boston Housing: (x_train, y_train), (x_test, y_test) = boston_housing.load_data()
print('x_train shape:', x_train.shape)
# La base de datos consta de 404 datos de entrenamiento y 102 de prueba, con 13 caracteristicas
# o entradas.
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')  # pre-procesamiento
mean = x_train.mean(0, keepdims=True)
dev = x_train.std(0, keepdims=True)
x_train = (x_train - mean) / dev
x_test = (x_test - mean) / dev
```

## Creación y entrenamiento de una red neuronal común

Creamos nuestra red neuronal de tres capas, 13 entradas y una salida (precio). 

```python
model = Sequential()

model.add(Dense(64, activation='relu', input_shape=input_shape))
model.add(Dense(64, activation='relu'))
model.add(Dense(1))

model.compile(optimizer='rmsprop', loss='mse', metrics=['mae'])

# Entrenamos y probamos el modelo en texto claro. Este es el procedimiento 
# normal sin ningún cifrado.
model.fit(x_train, y_train, batch_size=batch_size,
          epochs=epochs, verbose=1,
          validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=1)

print(score)
print('Test loss:', score[0])
print('Test mean absolute error:', score[1])  
# Guardamos los pesos del modelo para uso posterior.
model.save('housing-network.h5')  
```

## Cargar el modelo y realizar predicciones sin observar los datos de entrada

Con el modelo entrenado para predecir el precio medio de una casa basado en sus características, vamos a 
hacer predicciones con nuevos datos cifrados que no podemos observar. Para esto instanciamos de 
nuevo el modelo, pero ahora usando las funcionalidades de PySyft. 

```python
import tensorflow as tf
import syft as sy
# Importamos las librerias necesarias y luego envolvemos a keras usando PySyft. 
hook = sy.KerasHook(tf.keras)  model = Sequential()

# Ahora usamos el tamaño de datos del batch de entrada.
model.add(Dense(64, activation='relu', batch_input_shape=batch_input_shape))
model.add(Dense(64, activation='relu'))
model.add(Dense(1))  # Cargamos los pesos encontrados durante el entrenamiento.
pre_trained_weights = 'housing-network.h5'
model.load_weights(pre_trained_weights)  # Ahora creamos 3 working nodes de la librería TensorFlow Encrypted [4]. 
# Se necesitan al menos 3 trabajadores para poder utilizar la técnica de
# Secure Multi-party Computing.

# Este parámetro indica si queremos que PySyft maneje los trabajadores por nosotros.
# Esto es útil si queremos hacer una prueba donde todos los trabajadores están en la mismo
# máquina. En la vida real deberíamos usar máquina separadas.
AUTO = True

alice = sy.TFEWorker(host='localhost:4000', auto_managed=AUTO)
bob = sy.TFEWorker(host='localhost:4001', auto_managed=AUTO)
carol = sy.TFEWorker(host='localhost:4002', auto_managed=AUTO)

# Agrupamos los tres trabajadores en un cluster, el cual escucha request que contienen
# nuevos datos de entrada para predecir un precio, pero en este caso estos datos están cifrados.
cluster = sy.TFECluster(alice, bob, carol)
cluster.start()  # Cifrar y distribuir el modelo entre los tres trabajadores.
model.share(cluster)  # Activar el modelo para servir un máximo de 3 peticiones.
model.serve(num_requests=3)  
```

En este punto los trabajadores se encuentran escuchando por peticiones, y podemos proceder al la parte B [5] de este tutorial. 

## Finalmente detenemos los trabajadores.

```python
model.stop()
cluster.stop()

if not AUTO:
    process_ids = !ps aux | grep '[p]ython -m tf_encrypted.player --config' | awk '{print $2}'
    for process_id in process_ids:
        !kill {process_id}
        print("Process ID {id} has been killed.".format(id=process_id))  Referencias
```

* [1] https://gitlab.com/kuronosec/arhuaco/-/blob/devel/examples/Processing%20encrypted%20data%201%20-%20ES.ipynb
* [2] https://keras.io/
* [3] https://www.kaggle.com/c/boston-housing
* [4] https://tf-encrypted.io/
* [5] https://gitlab.com/kuronosec/arhuaco/-/blob/devel/examples/Processing%20encrypted%20data%202b%20-%20ES.ipynb
