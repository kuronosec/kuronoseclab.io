+++
author = "kurono"
title = "Vulnerabilidad encontrada en Tor"
date = "2014-08-20"
description = "Vulnerabilidad encontrada en Tor"
tags = [
    "security",
]
+++

# Vulnerabilidad encontrada en Tor

Tor y Tails son dos herramientas desarrolladas para mantener un nivel alto de privacidad mientras se utiliza Internet.  Fueron incluso utilizadas por Snowden para su propia protección, según lo informó el mismo [1]. Se les puede considerar el estado del arte en cuanto a tecnologías anti rastreo. Sin embargo, como todo sistema de cómputo, estas herramientas pensadas para un altísimo estándar de seguridad, tienen también sus propios agujeros. Recientemente se encontraron varias fallas de seguridad importantes, que fueron corregidas por los desarrolladores de los proyectos.

Tal como se describe en la pagina web del proyecto Tor [2], el 4 de julio de 2014 fueron detectados varios relays tratando de descubrir la identidad oculta de los usuarios que enviaban información a través de su red. Los objetivos del ataque eran especialmente los operadores de los hidden services, páginas web que se encuentran protegidas por la red Tor.

La técnica que fue usada por estos relays es conocida como ataque de confirmación de tráfico. Cuando un atacante tiene control sobre el inicio y el fin de un circuito dentro de la red (al fin y al cabo relays administrados por voluntarios) es posible para ellos medir diferentes características del tráfico que circula y obtener estadísticas valiosas a pesar que todo se encuentra cifrado. De esta manera pueden determinar si dos relays están en el mismo circuito. Si el primer relay en el circuito conoce la IP real del usuario y el último relay conoce la destinación de los recursos pedido por el usuario (como la página web a la que quiere acceder), entonces un atacante podría determinar con cierta probabilidad la fuente del tráfico (el usuario real). Este es un problema abierto sin solución efectiva hasta el momento. Sin embargo, si se asume que la mayoría de los relays en la red son benignos la probabilidad de que este ataque tenga éxito para una gran cantidad de usuarios es muy baja.

El tipo de ataque particular que se usó para vulnerar la red Tor utilizó lo explicado anteriormente, con la diferencia que se logró inyectar señales en los paquetes, siendo más efectivo para descubrir la IP real de los usuarios (porque se elimina el factor probabilístico). La idea es que un relay dentro de un circuito inyecte datos en las cabeceras del protocolo de Tor. El relay del otro lado del circuito busca estos datos y así se puede determinar fácilmente si los dos relays hacen parte del mismo circuito. De esta manera los atacantes inyectaban esta señal cuando el relay es usado como directorio de hidden services y buscaban las señal cuando el relay era un punto de entrada. Así se podía encontrar la identidad real de los usuarios que buscaban o publicaban información  sobre los hidden services. Cuando un cliente de Tor se conectaba a uno de estos relays atacantes, para acceder o publicar información sobre los hidden services, el relay inyectaba el nombre del hidden service codificado como un conjunto de paquetes del tipo relay y realy early. Otros relays podía leer esta información y determinar qué usuarios buscaban dichos hidden services y quienes publicaban información sobre ellos. Los Tor relays no deberían permitir el uso arbitrario de estos tipos de paquetes para la codificación de señales que revelan la verdadera identidad de un usuario o un servicio. Adicionalmente los atacantes usaron una técnica conocida como "Sybil attack", por media de la cual se utiliza una gran cantidad de recursos computacionales para suplantar un buen porcentaje de los relays en la red (cerca al 6% en este caso). Así se logra ampliar el impacto del ataque. Un problema adicional es que estas señales podían ser detectados por otras organizaciones como agencias estatales de inteligencia, lo que podría comprometer la privacidad de muchos de los usuarios.

Aun no es completamente claro el origen de este ataque y quién esta detrás del hallazgo de la vulnerabilidad. Sin embargo, es sabido que dos investigadores de Carnegie Mellon [3], quienes se disponían a realizar una presentación al respecto en el congreso de Black Hat 2014 titulada "You don't have to be the NSA to break Tor: de-anonymising users on a budget", podrían ser las personas detrás de los relays. La charla fue cancelada aparentemente por problemas legales [4]. Finalmente los investigadores compartieron parte de la información con los desarrolladores del proyecto Tor, lo que les permitió corregir el bug. Los relays deben actualizarse a la versión 0.2.4.23 ó 0.2.5.6-alpha en las que la vulnerabilidad se encuentra ya corregida, asumiendo que todo los detalles del bug fueron efectivamente informados.

Referencias:

* [1] - http://www.huffingtonpost.com/2013/07/18/tor-snowden_n_3610370.html

* [2] - https://blog.torproject.org/blog/tor-security-advisory-relay-early-traffic-confirmation-attack

* [3] - https://img.4plebs.org/boards/pol/image/1404/73/1404736805983.png

* [4] - http://www.theguardian.com/technology/2014/jul/22/is-tor-truly-anonymising-conference-cancelled
