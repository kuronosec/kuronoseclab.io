+++
author = "kurono"
title = "Divx heap overflow"
date = "2014-04-26"
description = "Divx heap overflow"
tags = [
    "security",
]
+++

# Divx plugin suite heap-based buffer overflow

DirectShowDemuxFilter as part of Divx plugin suite is vulnerable to heap-based buffer overflow, which can be exploited by malicious people to compromise a user's system. The vulnerability is caused due to a signedness error in the processing of "STRF" (Stream Format) chunks. This can be exploited to cause a heap-based buffer overflow via a specially crafted AVI file. Successful exploitation may allow execution of arbitrary code by tricking a user into visiting a malicious website.

DirectShowDemuxFilter is used by DivX Web Player, Divx Player and any application ussing Divx plugins like Windows Media Player (When Divx is installed). I have attached two avi files that trigger the vulnerability, the bug is caused when the offset 0x1159 ("STRF" Stream Format chunk) is altered:

when offset(0x1159) >= 8FFF, then we have the following error report

```bash
MSVCR80!memcpy+0x5a:
7814500a f3a5            rep movs dword ptr es:[edi],dword ptr [esi]

First Chance Exception Type: STATUS_ACCESS_VIOLATION (0xC0000005) Exception Sub-Type: Write Access Violation

Stack Trace:

MSVCR80!memcpy+0x5a
DMFContainer+0x146a4
DMFContainer!DSE::FileInspectorInterface::operator=+0x65cf
ntdll!RtlFreeHeapSlowly+0x5c2
MSVCR80!free+0xcd
Instruction Address: 0x000000007814500a
```

when offset(0x1159) < 8FFF, then we have the following error report

```bash
MSVCR80!LeadUpVec+0x52:
7814508e f3a5            rep movs dword ptr es:[edi],dword ptr [esi]

Exception Faulting Address: 0x1899000
First Chance Exception Type: STATUS_ACCESS_VIOLATION (0xC0000005)
Exception Sub-Type: Read Access Violation

Faulting Instruction:7814508e rep movs dword ptr es:[edi],dword ptr [esi]

Stack Trace:
MSVCR80!LeadUpVec+0x52
DirectShowDemuxFilter!DllUnregisterServer+0x20568
DirectShowDemuxFilter+0x223c
DirectShowDemuxFilter!DllUnregisterServer+0x955c
Instruction Address: 0x000000007814508e
```

These errors indicate that when we introduce a negative sign error we can overwrite regions on heap. When the sign is positive we can still crash the application, if we introduce one value big enough. I tested this bug in Windows 7 with all the patches applied and probably affects other versions.
