+++
author = "kurono"
title = "Multiple vulnerabilities in OpenCV"
date = "2013-06-14"
description = "Multiple vulnerabilities in OpenCV"
tags = [
    "security",
]
+++

I worked with OpenCv some time ago and I decided to carry out a little security audit. The tool used was flawfinder with the following command line:

```bash
flawfinder -m 3 --html --quiet --dataonly --context opencv-2.4.4/ > opencv-flaws.html
```

This analyses the source code and returns a lot of results, most of them false positive. But there are already several vulnerabilities I checked as real:

In the file opencv-2.4.4/modules/legacy/src/oneway.cpp line 1795:

```C
if (fscanf(pFile, "%s", imagename) <= 0) 
```

There is a buffer overflow when reading long files names from a list of files. For example when executing:

```bash
./c-example-one_way_sample location scene_l.bmp scene_r.bmp
Reading the images...
Extracted 42 keypoints...
Training one way descriptors...
buffer overflow detected ***: 
./c-example-one_way_sample terminated
```

This program reads a file called "one_way_train_images.txt" which its content is:

```bash
one_way_train_0000AAAAAAAAAAA...AAAAAAAAAAAAAAAAAAAAAAAAAA.jpg
one_way_train_0001BBBBBBBBBBB...BBBBBBBBBBBBBBBBBBBBBBBBBB.jpg
```

In the file opencv-2.4.4/modules/highgui/src/cap_images.cpp line 114:

```C
sprintf(str, filename, firstframe + currentframe);  
```

There is a format string when reading special files names. For example:

```bash
./c-example-adaptiveskindetector C:\VideoSequences\sample1\right_view\temp_%05d%n%s%s%s%s%s.jpg 0 1000
Press ESC to stop.
%n in writable segment detected ***
Abortado
```

These bugs could be exploited to execute arbitrary code.

Although of less concern, also there are vulnerabilities in the samples, here are two I found:

opencv-2.4.4/samples/cpp/hybridtrackingsample.cpp line 82, a buffer overflow:

```C
sprintf(test_file, "%s", argv[1]);
``` 

opencv-2.4.4/samples/cpp/hybridtrackingsample.cpp line 85 another buffer overflow:

```C
int values_read = fscanf(f, "%s\n", vid); 
```

# References: 

http://code.opencv.org/issues/2968
