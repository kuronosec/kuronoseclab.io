---
title: Team
subtitle: Parsec AI team
comments: false
---

### Andres Gomez Ramirez

![picture](/static/index.png)

I am a PhD in Computer Sciences with experience in the areas of computer security, machine learning, privacy and blockchain. I am exploring the topics of privacy applied to artificial intelligence and blockchain. I have contributed to the design, setup, and development of a safety mechanism for the computing infrastructure of the ALICE experiment, part of the Large Hadron Collider at CERN.
